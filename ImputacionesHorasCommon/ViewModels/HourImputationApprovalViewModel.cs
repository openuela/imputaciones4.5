﻿using ImputacionesHorasCommon.Models;
using System.Collections.Generic;


namespace ImputacionesHorasCommon.ViewModels
{
    public class HourImputationApprovalViewModel
    {
        public List<ApprovalTypes> ApprovalTypes { get; set; }
        public List<Jobs> Projects { get; set; }
        public List<HourImputation> HourImputations { get; set; }
    }
}
