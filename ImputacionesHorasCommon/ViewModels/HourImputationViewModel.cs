﻿using ImputacionesHorasCommon.Models;
using System.Collections.Generic;

namespace ImputacionesHorasCommon.ViewModels
{
    public class HourImputationViewModel
    {
        public HourImputationFilter HourImputationFilter { get; set; }
        public List<HourImputation> HourImputations { get; set; }
    }
}
