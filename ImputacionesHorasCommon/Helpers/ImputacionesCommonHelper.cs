﻿using System.ComponentModel;
using System;

namespace ImputacionesHorasCommon.Helpers
{
    public static class ImputacionesCommonHelper
    {
        public enum EmailType
        {
            [Description("Registro nuevo usuario")]
            Usuario
        }

        /// <summary>
        /// Obtener la descripción del enumerador.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string strGetEnumDescription(this Enum value)
        {
            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length == 0 ? value.ToString() : ((DescriptionAttribute)attributes[0]).Description;
        }
    }
}
