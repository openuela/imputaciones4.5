﻿using System.ComponentModel.DataAnnotations;

namespace ImputacionesHorasCommon.Models
{
    public class RegisterUser
    {
        [Required]
        [Display(ResourceType = typeof(Resources.Resource), Name = "FirstName")]
        public string FirstName { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Resource), Name = "LastName")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        [Display(ResourceType = typeof(Resources.Resource), Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Resource), Name = "UserID")]
        public string UserName { get; set; }

        [Required]
        [StringLength(6, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "Password")]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ConfirmPassword")]
        [Compare("Password", ErrorMessage = "La contraseña y la confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }
    }
}
