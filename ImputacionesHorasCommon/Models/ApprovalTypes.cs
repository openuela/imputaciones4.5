﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImputacionesHorasCommon.Models
{
    public class ApprovalTypes
    {
        public int ID { get; set; }

        public string ApprovalType { get; set; }
    }
}
