﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImputacionesHorasCommon.Models
{
    public class HourImputationFilter
    {
        [Display(ResourceType = typeof(Resources.Resource), Name = "InputDate")]
        [DataType(DataType.Date)]
        [DisplayFormat(NullDisplayText = "", DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? InputDate { get; set; }

        [StringLength(20)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ProjectNo")]
        public string ProjectNo { get; set; }

        [StringLength(20)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ProjectTaskNo")]
        public string ProjectTaskNo { get; set; }

        [Display(ResourceType = typeof(Resources.Resource), Name = "PlanningLineNo")]
        public int PlanningLineNo { get; set; }

        [StringLength(10)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "WorkType")]
        public string WorkType { get; set; }

        [StringLength(10)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "UoM")]
        public string UnitOfMeasure { get; set; }

        [Display(ResourceType = typeof(Resources.Resource), Name = "Price")]
        [Column(TypeName = "decimal")]
        public decimal Price { get; set; }

        [Display(ResourceType = typeof(Resources.Resource), Name = "Qty")]
        [Column(TypeName = "decimal")]
        public decimal Qty { get; set; }

        [StringLength(20)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ResponsiblePerson")]
        public string ResponsiblePerson { get; set; }

        [NotMapped]
        public List<Jobs> JobsCollection { get; set; }

        [NotMapped]
        public List<JobsTask> JobsTaskCollection { get; set; }

        [NotMapped]
        public List<JobsTaskLines> PlanningLinesCollection { get; set; }

        [NotMapped]
        public List<WorkTypes> WorkTypesCollection { get; set; }
    }
}
