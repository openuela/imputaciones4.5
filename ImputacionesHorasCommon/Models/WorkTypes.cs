﻿using System.ComponentModel.DataAnnotations;

namespace ImputacionesHorasCommon.Models
{
    public class WorkTypes
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [StringLength(10)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "WorkTypeCode")]
        public string WorkTypeCode { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "WorkType")]
        public string WorkTypeDescription { get; set; }

        [Required]
        [StringLength(10)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "UoM")]
        public string UoM { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ShowPrice")]
        public bool ShowPrice { get; set; }

    }
}
