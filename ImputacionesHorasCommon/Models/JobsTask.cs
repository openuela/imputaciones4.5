﻿namespace ImputacionesHorasCommon.Models
{
    public class JobsTask
    {
        public string ProjectTaskNo { get; set; }

        public string ProjectTaskDescription { get; set; }
    }
}
