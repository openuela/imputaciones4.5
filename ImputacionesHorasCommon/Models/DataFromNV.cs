﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImputacionesHorasCommon.Models
{
    public class DataFromNV
    {
        [Key]
        public int ID { get; set; }

        [StringLength(20)]
        public string ProjectNo { get; set; }

        [StringLength(100)]
        public string ProjectDescription { get; set; }

        public int Status { get; set; }

        public int Blocked { get; set; }

        [StringLength(20)]
        public string ProjectTaskNo { get; set; }

        [StringLength(50)]
        public string ProjectTaskDescription { get; set; }

        public int PlanningLineNo { get; set; }

        public int PlanningLineType { get; set; }

        [StringLength(50)]
        public string PlanningLineDescription { get; set; }

        [StringLength(10)]
        public string WorkTypeCode { get; set; }

        [StringLength(10)]
        public string UoM { get; set; }

        public decimal Price { get; set; }

        [StringLength(20)]
        public string User { get; set; }

        [StringLength(20)]
        public string ResponsiblePerson { get; set; }

    }
}
