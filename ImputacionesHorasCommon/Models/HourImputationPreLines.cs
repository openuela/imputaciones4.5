﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ImputacionesHorasCommon.Models
{
    public class HourImputationPreLines
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Resource), Name = "InputDate")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime InputDate { get; set; }

        [Required]
        [StringLength(20)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ProjectNo")]
        public string ProjectNo { get; set; }

        [Required]
        [StringLength(100)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ProjectDescription")]
        public string ProjectDescription { get; set; }

        [Required]
        [StringLength(20)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ProjectTaskNo")]
        public string ProjectTaskNo { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ProjectTaskDescription")]
        public string ProjectTaskDescription { get; set; }

        [Required]
        [Display(ResourceType = typeof(Resources.Resource), Name = "PlanningLineNo")]
        public int PlanningLineNo { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "PlanningLineNo")]
        public string PlanningLineDescription { get; set; }

        //[Required] Momentáneo
        [StringLength(10)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "WorkType")]
        public string WorkType { get; set; }

        [Required]
        [StringLength(50)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "WorkType")]
        public string WorkTypeDescription { get; set; }

        [StringLength(10)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "UoM")]
        public string UnitOfMeasure { get; set; }

        [Display(ResourceType = typeof(Resources.Resource), Name = "Price")]
        [Column(TypeName = "decimal")]
        public decimal Price { get; set; }

        [Display(ResourceType = typeof(Resources.Resource), Name = "Qty")]
        [Column(TypeName = "decimal")]
        public decimal Qty { get; set; }

        [Required]
        [StringLength(20)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "User")]
        public string User { get; set; }

        [Required]
        [StringLength(20)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "ResponsiblePerson")]
        public string ResponsiblePerson { get; set; }
    }
}
