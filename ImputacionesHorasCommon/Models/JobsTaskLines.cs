﻿namespace ImputacionesHorasCommon.Models
{
    public class JobsTaskLines
    {
        public int PlanningLineNo { get; set; }

        public string PlanningLineDescription { get; set; }

        public string WorkTypeCode { get; set; }

        public string WorkTypeDescription { get; set; }

        public string UnitOfMeasure { get; set; }

        public decimal Price { get; set; }
    }
}
