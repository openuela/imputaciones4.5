﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ImputacionesHorasCommon.Models
{
    public class Login
    {
        [Required]
        [Display(ResourceType = typeof(Resources.Resource), Name = "UserID")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(Resources.Resource), Name = "Password")]
        public string Password { get; set; }
    }
}
