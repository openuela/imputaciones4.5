﻿using ImputacionesHorasCommon.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;


namespace ImputacionesHorasDomain.Context
{
    public class DataContext:DbContext
    {
        public DataContext()
            :base("DefaultConnection")
        {

        }

        public DbSet<HourImputation> HourImputation { get; set; }
        public DbSet<DataFromNV> DataFromNV { get; set; }
        public DbSet<HourImputationPreLines> HourImputationPreLines { get; set; }
        public DbSet<WorkTypes> WorkTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<HourImputation>().Property(h => h.Price).HasPrecision(38, 20);
            modelBuilder.Entity<DataFromNV>().Property(d => d.Price).HasPrecision(38, 20);
            modelBuilder.Entity<HourImputationPreLines>().Property(d => d.Price).HasPrecision(38, 20);

        }
    }
}
