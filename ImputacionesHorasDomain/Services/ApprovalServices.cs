﻿using ImputacionesHorasCommon.Models;
using ImputacionesHorasDomain.Context;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Data.SqlClient;

namespace ImputacionesHorasDomain.Services
{
    public class ApprovalServices
    {
        /// <summary>
        /// Devuelve TODAS las imputaciones pendientes de aprobación
        /// </summary>
        /// <param name="responsible"></param>
        /// <returns></returns>
        public static List<HourImputation> GetAllImputationsLines(string responsible)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.HourImputation.Where(h => h.ResponsiblePerson == responsible &&
                    h.Approved != true).OrderBy(h => h.User).ThenBy(h => h.ProjectNo).ThenBy(h => h.InputDate).ToList();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        /// <summary>
        /// Actualiza a aprobadas las líneas de imputación.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="approval"></param>
        /// <returns></returns>
        public static Response ProcessApproval(int id, bool approval, decimal qty)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var line = dbContext.HourImputation.Where(h => h.ID == id).FirstOrDefault();
                    line.Approved = approval;
                    line.ApprovalDate = DateTime.Now;
                    line.Qty = qty;

                    dbContext.Entry(line).CurrentValues.SetValues(line);
                    dbContext.SaveChanges();

                    return new Response
                    {
                        IsSuccess = true,
                    };
                }
                catch (Exception ex)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = ex.Message
                    };
                }
            }
        }


        /// <summary>
        /// Obtiene las líneas de imputación según el tipo de aprobación
        /// </summary>
        /// <param name="responsible"></param>
        /// <param name="typeApproval">0 = Todas / 1 = Horas / 2 = Gastos</param>
        /// <returns></returns>
        public static List<HourImputation> GetImputationsLinesByType(string responsible, int typeApproval)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var query = "SELECT HI.* FROM HourImputation HI " +
                                "INNER JOIN WorkTypes WT " +
                                "ON HI.WorkType = WT.WorkTypeCode " +
                                "WHERE ResponsiblePerson = @Responsible "+
                                "   AND HI.Approved = 0 ";
                    switch (typeApproval)
                    {
                        case 1:
                            query = query + "AND WT.ShowPrice = 0 ";
                            break;
                        case 2:
                            query = query + "AND WT.ShowPrice = 1 ";
                            break;
                    }

                    var list = dbContext.Database.SqlQuery<HourImputation>(
                        query, new SqlParameter("@Responsible", responsible),
                               new SqlParameter("@TypeApproval", typeApproval)).ToList();
                    list = list.OrderBy(l => l.User).ThenBy(l => l.InputDate).ToList();
                    return list;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="responsible"></param>
        /// <param name="projectNo"></param>
        /// <returns></returns>
        public static List<HourImputation> GetImputationsLinesByProject(string responsible, string projectNo)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.HourImputation.Where(h => h.ResponsiblePerson == responsible &&
                    h.ProjectNo.Contains(projectNo)).ToList();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        /// <summary>
        /// Devuelve una lista estática de los tipos de aprobación.
        /// </summary>
        /// <returns></returns>
        public static List<ApprovalTypes> GetApprovalTypes()
        {
            var approvalTypes = new List<ApprovalTypes>()
            {
                new ApprovalTypes()
                {
                    ID = 0,
                    ApprovalType = "Todas"
                },
                new ApprovalTypes()
                {
                    ID = 1,
                    ApprovalType = "Horas"
                },
                new ApprovalTypes()
                {
                    ID = 2,
                    ApprovalType = "Gastos"
                }
            };

            return approvalTypes;
        }
    }

}
