﻿using ImputacionesHorasCommon.Models;
using ImputacionesHorasDomain.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace ImputacionesHorasDomain.Services
{
    public class HourImputationServices
    {
        public static Response AddImputationHourLine(HourImputationPreLines preLine)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    dbContext.HourImputationPreLines.Add(preLine);
                    dbContext.SaveChanges();
                    return new Response
                    {
                        IsSuccess = true,
                        Result = preLine.ID
                    };
                }
                catch (Exception ex)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = ex.Message
                    };
                }
            }
        }

        public static void DeleteImputationsHourPreLines(string  user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var preLines = (from n in dbContext.HourImputationPreLines
                                    where n.User == user
                                    select n).ToList();

                    dbContext.HourImputationPreLines.RemoveRange(preLines);
                    dbContext.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        public static List<HourImputation> GetImputationHourPreLines(string user, bool showPrice)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                   return  dbContext.Database.SqlQuery<HourImputation>(
                                @"SELECT    HPL.ID,
                                            HPL.InputDate,
                                            HPL.ProjectNo,
		                                    HPL.ProjectDescription,
		                                    HPL.ProjectTaskNo,
		                                    HPL.ProjectTaskDescription,
                                            HPL.PlanningLineNo,
                                            HPL.PlanningLineDescription,
		                                    HPL.WorkType,
		                                    HPL.WorkTypeDescription,
                                            HPL.UnitOfMeasure,
                                            HPL.Price,
		                                    HPL.Qty,
                                            null AS Approved,
                                            null AS ApprovalDate,
		                                    HPL.[User],
		                                    HPL.ResponsiblePerson
                                FROM HourImputationPreLines HPL
                                INNER JOIN WorkTypes WT
                                ON HPL.WorkType = WT.WorkTypeCode 
                                    AND ShowPrice = @ShowPrice
                                WHERE HPL.[User] = @User"
                                    , new SqlParameter("@User", user),
                                      new SqlParameter("@ShowPrice", showPrice)).ToList();
                }
                catch (Exception)
                {
                    throw;
                }
            }      
        }


        public static Response DeleteImputationHourLine(int id)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var line = (from n in dbContext.HourImputationPreLines
                                where n.ID == id
                                select n).FirstOrDefault();
                    dbContext.HourImputationPreLines.Remove(line);
                    dbContext.SaveChanges();
                    return new Response
                    {
                        IsSuccess = true
                    };
                }
                catch (Exception ex)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = ex.Message
                    };
                }
            }
        }

        public static Response AddHourImputations (List<HourImputation> hourImputations)
        {
            using (DataContext dbContext = new DataContext())
                try
                {
                    foreach (var item in hourImputations)
                    {
                        item.Approved = false;

                        //PQC: Mientras se resuelve el tema del WorkType ""
                        if (item.WorkType == null)
                        {
                            item.WorkType = string.Empty;
                        }
                    }

                    //Se adiciona los registros a la entidad HourImputation
                    dbContext.HourImputation.AddRange(hourImputations);

                    //Se colocan los registros para eliminar de la tabla temporal.
                    foreach (var item in hourImputations)
                    {
                        var line = (from n in dbContext.HourImputationPreLines
                                    where n.ID == item.ID
                                    select n).FirstOrDefault();

                        dbContext.HourImputationPreLines.Remove(line);
                    }
                    //Se guardan los cambios.
                    dbContext.SaveChanges();

                    return new Response
                    {
                        IsSuccess = true
                    };
                }
                catch (Exception ex)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = ex.Message
                    };
                }
        }
    }
}
