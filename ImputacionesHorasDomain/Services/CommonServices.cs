﻿using System;
using ImputacionesHorasCommon.Models;
using ImputacionesHorasDomain.Context;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;

namespace ImputacionesHorasDomain.Services
{
    public class CommonServices
    {
        #region Jobs
        /// <summary>
        /// Obtiene los proyectos según el usuario logueado, sín los no aceptados ni los finalizados.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Jobs> GetJobs(string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var jobs = dbContext.Database.SqlQuery<Jobs>(
                        @"SELECT DISTINCT ProjectNo, ProjectNo + ' - ' + ProjectDescription AS ProjectDescription
                          FROM DataFromNV
                          WHERE [User] = @User
                            AND [Status] NOT IN (3,4)
                                AND Blocked <> 2", new SqlParameter("@User", user)).ToList();
                    return jobs;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene los proyectos con líneas de planificación de gastos según el usuario logueado, 
        /// sín los no aceptados ni los finalizados.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Jobs> GetJobsExpenses(string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var jobs = dbContext.Database.SqlQuery<Jobs>(
                        @"SELECT DISTINCT ProjectNo, ProjectNo + ' - ' + ProjectDescription AS ProjectDescription
                          FROM DataFromNV NV
                          INNER JOIN WorkTypes WT
                          ON NV.WorkTypeCode = WT.WorkTypeCode
	                        AND WT.ShowPrice = 1  
                          WHERE [User] = @User
                            AND [Status] NOT IN (3,4)
                                AND Blocked <> 2", new SqlParameter("@User", user)).ToList();
                    return jobs;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Devuelve todos los proyectos, incluyendo los que ya están finalizados.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Jobs> GetJobsFinished(string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var jobs = dbContext.Database.SqlQuery<Jobs>(
                        @"SELECT DISTINCT ProjectNo, ProjectNo + ' - ' + ProjectDescription AS ProjectDescription
                          FROM DataFromNV
                          WHERE [User] = @User
                            AND [Status] NOT IN (4)
                                AND Blocked <> 2", new SqlParameter("@User", user)).ToList();
                    return jobs;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene los proyectos con líneas de planificación de gastos según el usuario logueado, 
        /// sín los no aceptados pero con los finalizados.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Jobs> GetJobsExpensesFinished(string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var jobs = dbContext.Database.SqlQuery<Jobs>(
                        @"SELECT DISTINCT ProjectNo, ProjectNo + ' - ' + ProjectDescription AS ProjectDescription
                          FROM DataFromNV NV
                          INNER JOIN WorkTypes WT
                          ON NV.WorkTypeCode = WT.WorkTypeCode
	                        AND WT.ShowPrice = 1  
                          WHERE [User] = @User
                            AND [Status] NOT IN (4)
                                AND Blocked <> 2", new SqlParameter("@User", user)).ToList();
                    return jobs;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene los proyectos según el supervisor logueado de la tabla imputaciones
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Jobs> GetJobsForApproval(string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var jobs = dbContext.Database.SqlQuery<Jobs>(
                        @"SELECT DISTINCT ProjectNo, ProjectDescription
                          FROM HourImputation
                          WHERE [ResponsiblePerson] = @User"
                        , new SqlParameter("@User", user)).ToList();
                    return jobs;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="responsible"></param>
        /// <param name="typeApproval"></param>
        /// <returns></returns>
        public static List<Jobs> GetProjectsByType(string responsible, int typeApproval)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    var sqlQuery = "SELECT DISTINCT ProjectNo, ProjectDescription " +
                                   "FROM HourImputation HI " +
                                   "INNER JOIN WorkTypes WT " +
                                   "ON HI.WorkType = WT.WorkTypeCode " +
                                   "WHERE [ResponsiblePerson] = @User ";

                    switch (typeApproval)
                    {
                        case 1:
                            sqlQuery = sqlQuery + "AND WT.ShowPrice = 0 ";
                            break;
                        case 2:
                            sqlQuery = sqlQuery + "AND WT.ShowPrice = 1 ";
                            break;
                    }

                    var jobs = dbContext.Database.SqlQuery<Jobs>(
                        sqlQuery, new SqlParameter("@User", responsible)).ToList();
                    return jobs;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }


        #endregion

        #region Tasks
        /// <summary>
        /// Obtiene las tareas según el proyecto seleccionado y el usuario logueado
        /// </summary>
        /// <param name="project"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<JobsTask> GetJobsTasks(string project, string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.Database.SqlQuery<JobsTask>(
                        @"SELECT DISTINCT ProjectTaskNo, ProjectTaskDescription
                          FROM DataFromNV
                          WHERE ProjectNo = @Project
                            AND [User] = @User", new SqlParameter("@Project", project),
                                                 new SqlParameter("@User", user)).ToList();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene las tareas según el proyecto seleccionado y el usuario logueado
        /// filtrando solo las tareas que poseen gastos
        /// </summary>
        /// <param name="project"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<JobsTask> GetJobTasksExpenses(string project, string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.Database.SqlQuery<JobsTask>(
                        @"SELECT DISTINCT ProjectTaskNo, ProjectTaskDescription
                          FROM DataFromNV NV
                          INNER JOIN WorkTypes WT
                          ON NV.WorkTypeCode = WT.WorkTypeCode
	                        AND WT.ShowPrice = 1  
                          WHERE ProjectNo = @Project
                            AND [User] = @User", new SqlParameter("@Project", project),
                                                 new SqlParameter("@User", user)).ToList();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        #endregion

        #region PlanningLines
        /// <summary>
        /// Obtiene las líneas de planificación
        /// </summary>
        /// <param name="project"></param>
        /// <param name="task"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<JobsTaskLines> GetPlanningLines(string project, string task, string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.Database.SqlQuery<JobsTaskLines>(
                        @"SELECT	PlanningLineNo, 
		                            PlanningLineDescription
                          FROM DataFromNV
                          WHERE ProjectNo = @Project 
	                        AND ProjectTaskNo = @Task
		                        AND [User] = @User
			                        AND PlanningLineType = 0", new SqlParameter("@Project", project),
                                                               new SqlParameter("@Task", task),
                                                               new SqlParameter("@User", user)).ToList();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        /// <summary>
        /// Obtiene las líneas de planificación que corresponden a imputaciones de gastos
        /// </summary>
        /// <param name="project"></param>
        /// <param name="task"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<JobsTaskLines> GetPlanningLinesExpenses(string project, string task, string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.Database.SqlQuery<JobsTaskLines>(
                        @"SELECT	PlanningLineNo, 
		                            PlanningLineDescription
                          FROM DataFromNV NV
                          INNER JOIN WorkTypes WT
                          ON NV.WorkTypeCode = WT.WorkTypeCode
	                        AND WT.ShowPrice = 1
                          WHERE ProjectNo = @Project 
	                        AND ProjectTaskNo = @Task
		                        AND [User] = @User
			                        AND PlanningLineType = 0", new SqlParameter("@Project", project),
                                                               new SqlParameter("@Task", task),
                                                               new SqlParameter("@User", user)).ToList();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        #endregion

        #region WorkTypes
        /// <summary>
        /// Obtiene todos los tipos de trabajos disponibles
        /// </summary>

        public static List<WorkTypes> GetWorkTypes()
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.WorkTypes.ToList();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }
        #endregion

        #region WorkTypesUoM
        public static JobsTaskLines GetWorkTypeUoM(string project, string task, int planningLines, string user)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.Database.SqlQuery<JobsTaskLines>(
                        @"SELECT	PlanningLineNo,
                                    PlanningLineDescription,
                                    NV.WorkTypeCode,
                                    ISNULL(WorkTypeDescription,'No definido') as WorkTypeDescription,
		                            NV.UoM AS UnitOfMeasure,
                                    NV.Price
                          FROM DataFromNV NV
                          LEFT JOIN WorkTypes WT
                          ON NV.WorkTypeCode = WT.WorkTypeCode   
                          WHERE ProjectNo = @Project 
	                        AND ProjectTaskNo = @Task
		                        AND [User] = @User
                                    AND PlanningLineNo = @PlanningLines
			                            AND PlanningLineType = 0", new SqlParameter("@Project", project),
                                                                   new SqlParameter("@Task", task),
                                                                   new SqlParameter("@User", user),
                                                                   new SqlParameter("@PlanningLines", planningLines)).FirstOrDefault();
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        #endregion

        #region UoM
        /// <summary>
        /// Obtiene la unidad de medida segun el usuario logueado, el proyecto, tarea y línea 
        /// de planificación seleccionados.
        /// </summary>
        /// <param name="workType">TipoTrabajo</param>
        /// 
        public static string GetUoM(string workType)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.Database.SqlQuery<string>(
                        @"SELECT UoM FROM WorkTypes
                          WHERE WorkTypeCode = @WorkType", new SqlParameter("@WorkType", workType)).FirstOrDefault();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Responsible

        /// <summary>
        /// Obtiene la persona responsable.
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        public static string GetResponsible(string project)
        {
            using (DataContext dbContext = new DataContext())
            {
                try
                {
                    return dbContext.Database.SqlQuery<string>(
                        @"SELECT ResponsiblePerson FROM DataFromNV
                          WHERE ProjectNo = @Project", new SqlParameter("@Project", project)).FirstOrDefault();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion
    }
}
