﻿using ImputacionesHorasCommon.Models;
using ImputacionesHorasCommon.ViewModels;
using ImputacionesHorasDomain.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ImputacionesHoras.Controllers
{
    [Authorize]
    public class HourImputationController : Controller
    {

        // GET: HourImputation/Create
        #region Create
        public ActionResult Create()
        {

            //Con esta línea, puedo usar el ApplicationUserManager en cualquier controlador
            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var user = userManager.FindByName(User.Identity.Name);

            ViewBag.LastInputDate = user.LastInputDate.Value.ToString("yyyy-MM-dd");
            ViewBag.User = user.UserName;

            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobsServices(ViewBag.User);
            hourImputations.HourImputationFilter.InputDate = DateTime.Now;

            hourImputations.HourImputations = 
                HourImputationServices.GetImputationHourPreLines(ViewBag.User, false);

            return View(hourImputations);
        }

        //POST: HourImputation/Create
        [HttpPost]
        public ActionResult Create (HourImputationViewModel imputationViewModel)
        {

            if (imputationViewModel.HourImputations == null)
            {
                ViewBag.User = User.Identity.Name;

                imputationViewModel.HourImputationFilter = GetJobsServices(ViewBag.User);
                imputationViewModel.HourImputationFilter.InputDate = DateTime.Now;
                return View(imputationViewModel);
            }

            var response = HourImputationServices.AddHourImputations(imputationViewModel.HourImputations);

            if (!response.IsSuccess)
            {
                ViewBag.User = User.Identity.Name;
                imputationViewModel.HourImputationFilter = GetJobsServices(ViewBag.User);
                imputationViewModel.HourImputationFilter.InputDate = null;
                return View(imputationViewModel);
            }

            //Con esta línea, puedo usar el ApplicationUserManager en cualquier controlador
            var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            
            //Buscar el usuario por el nombre
            var user = userManager.FindByName(User.Identity.Name);

            //Buscar la fecha máxima de la lista que se procesó previamente
            var maxDate = imputationViewModel.HourImputations.Max(h => h.InputDate);

            //Se asigna el valor de esa fecha al campo correspondiente  del usuario y se actualiza.
            user.LastInputDate = maxDate;
            userManager.Update(user);

            //Se retorna a la vista con el modelo vacío y se envía el valor del success para manejar los mensajes
            ViewBag.IsSuccess = response.IsSuccess.ToString();
            user = userManager.FindByName(User.Identity.Name);
            ViewBag.User = user.UserName;
            ViewBag.LastInputDate = user.LastInputDate.Value.ToString("yyyy-MM-dd");

            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobsServices(user.UserName);
            hourImputations.HourImputationFilter.InputDate = null;
            return View(hourImputations);
        }
        #endregion

        #region GetJobs
        [HttpPost]
        public PartialViewResult GetJobsFinished(string user)
        {
            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobsFinishedServices(user);
            return PartialView("JobPartial", hourImputations);
        }

        private HourImputationFilter GetJobsFinishedServices(string user)
        {
            var hourImputation = new HourImputationFilter();
            var jobsFinished = CommonServices.GetJobsFinished(user);
            hourImputation.JobsCollection = jobsFinished;
            return hourImputation;
        }

        [HttpPost]
        public PartialViewResult GetCurrentJobs(string user)
        {
            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobsServices(user);
            return PartialView("JobPartial", hourImputations);
        }

        private HourImputationFilter GetJobsServices(string user)
        {
            var hourImputation = new HourImputationFilter();
            var jobs = CommonServices.GetJobs(user);
            var WorkTypes = CommonServices.GetWorkTypes();
            hourImputation.JobsCollection = jobs;
            hourImputation.WorkTypesCollection = WorkTypes;
            return hourImputation;
        }
        #endregion

        #region GetTask
        [HttpPost]
        public PartialViewResult GetJobTasks(string project, string user)
        {
            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobTaskServices(project, user);

            return PartialView("TaskPartial", hourImputations);
        }

        private HourImputationFilter GetJobTaskServices(string project, string user)
        {
            var hourImputation = new HourImputationFilter();
            var jobTasks = CommonServices.GetJobsTasks(project, user);            
            hourImputation.JobsTaskCollection = jobTasks;
            hourImputation.ResponsiblePerson = CommonServices.GetResponsible(project);

            return hourImputation;
        }
        #endregion

        #region PlanningLines
        [HttpPost]
        public PartialViewResult GetPlanningLines(string project, string task, string user)
        {
            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetPlanningLinesServices(project, task, user);

            return PartialView("PlanningLinesPartial", hourImputations);
        }

        private HourImputationFilter GetPlanningLinesServices(string project, string task, string user)
        {
            var hourImputation = new HourImputationFilter();
            var planningLines = CommonServices.GetPlanningLines(project, task, user);
            hourImputation.PlanningLinesCollection = planningLines;
            return hourImputation;
                
        }
        #endregion

        #region GetUoM
        [HttpPost]
        public JsonResult GetWorkTypeUoM(string project, string task, int planningLines, string user)
        {
            var worktypeUoM = new JobsTaskLines();
            worktypeUoM = CommonServices.GetWorkTypeUoM(project, task, planningLines, user);
            var result = new
            {
                WorkType = worktypeUoM.WorkTypeCode,
                WorkTypeDescription = worktypeUoM.WorkTypeDescription, 
                UoM = worktypeUoM.UnitOfMeasure,
                Price = worktypeUoM.Price
            };
            return Json(result);

        }

        private HourImputationFilter GetUoMServices(string workType)
        {
            var hourImputation = new HourImputationFilter();
            var uom = CommonServices.GetUoM(workType);
            hourImputation.UnitOfMeasure = uom;
            return hourImputation;
        }
        #endregion

        #region LoadPartialView
        [HttpPost]
        public ActionResult AddImputationHourLine(DateTime inputDate, string project, string projectDescription,
            string task, string taskDescription, int planningLineNo, string planningLineDescription, string worktypecode, 
            string workTypeDescription, string UoM, decimal qty, string user, string responsible)
        {
            var hourImputationLine = new HourImputationPreLines
            {
                InputDate = inputDate,
                ProjectNo = project,
                ProjectDescription = projectDescription,
                ProjectTaskNo = task,
                ProjectTaskDescription = taskDescription,
                PlanningLineNo = planningLineNo,
                PlanningLineDescription = planningLineDescription,
                WorkType = worktypecode,
                WorkTypeDescription = workTypeDescription,
                UnitOfMeasure = UoM,
                Price = 0,
                Qty = qty,
                User = user,
                ResponsiblePerson = user
            };

            var response = HourImputationServices.AddImputationHourLine(hourImputationLine);

            if (!response.IsSuccess)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            var hourImputations = new HourImputationViewModel();

            hourImputations.HourImputations = 
                HourImputationServices.GetImputationHourPreLines(user, false);

            return PartialView("_HourImputationsList", hourImputations);
        }

        [HttpPost]
        public ActionResult DeleteImputationHourLine (int id)
        {
            var user = User.Identity.Name;

            var response = HourImputationServices.DeleteImputationHourLine(id);

            //if (!response.IsSuccess)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            //}

            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputations = 
                HourImputationServices.GetImputationHourPreLines(user, false);

            return PartialView("_HourImputationsList", hourImputations);
        }
        #endregion

    }
}
