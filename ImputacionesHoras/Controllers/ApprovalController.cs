﻿using ImputacionesHorasCommon.ViewModels;
using ImputacionesHorasDomain.Services;
using System.Web.Mvc;

namespace ImputacionesHoras.Controllers
{
    [Authorize]
    public class ApprovalController : Controller
    {
        // GET: Approval/Create
        public ActionResult Create()
        {
            var user = User.Identity.Name;
            ViewBag.User = user;

            var hourImpuations = new HourImputationApprovalViewModel();
            hourImpuations.ApprovalTypes = ApprovalServices.GetApprovalTypes();
            hourImpuations.Projects = CommonServices.GetJobsForApproval(user);
            hourImpuations.HourImputations = ApprovalServices.GetAllImputationsLines(user);

            return View(hourImpuations);
        }

        [HttpPost]
        public JsonResult ProcessApproval (int id, bool approval, decimal qty)
        {
            var response = ApprovalServices.ProcessApproval(id, approval, qty);
            return Json(response);
        }

        [HttpPost]
        public PartialViewResult GetLinesForApproval(string responsible)
        {
            var hourImpuations = new HourImputationApprovalViewModel();
            hourImpuations.ApprovalTypes = ApprovalServices.GetApprovalTypes();
            hourImpuations.HourImputations = ApprovalServices.GetAllImputationsLines(responsible);
            return PartialView("_ApprovalList", hourImpuations);
        }

        [HttpPost]
        public PartialViewResult GetApprovalLinesByType(string responsible, int typeApproval)
        {
            var hourImputations = new HourImputationApprovalViewModel();
            hourImputations.ApprovalTypes = ApprovalServices.GetApprovalTypes();
            hourImputations.HourImputations = ApprovalServices.GetImputationsLinesByType(responsible, typeApproval);
            return PartialView("_ApprovalList", hourImputations);
        }

        [HttpPost]
        public PartialViewResult GetProjectsForApprovalByType (string responsible, int typeApproval)
        {
            var hourImputations = new HourImputationApprovalViewModel();
            hourImputations.Projects = CommonServices.GetProjectsByType(responsible, typeApproval);
            return PartialView("JobPartialByType", hourImputations);
        }

        [HttpPost]
        public PartialViewResult GetLinesforApprovalByProject(string responsible, string projectNo)
        {
            var hourImputations = new HourImputationApprovalViewModel();
            hourImputations.ApprovalTypes = ApprovalServices.GetApprovalTypes();
            hourImputations.HourImputations = ApprovalServices.GetImputationsLinesByProject(responsible, projectNo);
            return PartialView("_ApprovalList", hourImputations);
        }
    }
}