﻿using ImputacionesHorasCommon.Models;
using ImputacionesHorasCommon.ViewModels;
using ImputacionesHorasDomain.Services;
using System;
using System.Net;
using System.Web.Mvc;

namespace ImputacionesHoras.Controllers
{
    [Authorize]
    public class ExpensesImputationController : Controller
    {
        #region Create
        //GET: ExpensesImputation/Create
        public ActionResult Create()
        {
            ViewBag.User = User.Identity.Name;

            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobsServices(ViewBag.User);
            hourImputations.HourImputationFilter.InputDate = DateTime.Now;
            hourImputations.HourImputations =
                HourImputationServices.GetImputationHourPreLines(ViewBag.User, true);

            return View(hourImputations);
        }

        //POST: ExpensesImputation/Create
        [HttpPost]
        public ActionResult Create(HourImputationViewModel imputationViewModel)
        {

            if (imputationViewModel.HourImputations == null)
            {
                ViewBag.User = User.Identity.Name;

                imputationViewModel.HourImputationFilter = GetJobsServices(ViewBag.User);
                imputationViewModel.HourImputationFilter.InputDate = DateTime.Now;
                return View(imputationViewModel);
            }

            var response = HourImputationServices.AddHourImputations(imputationViewModel.HourImputations);

            if (!response.IsSuccess)
            {
                ViewBag.User = User.Identity.Name;
                imputationViewModel.HourImputationFilter = GetJobsServices(ViewBag.User);
                imputationViewModel.HourImputationFilter.InputDate = null;
                return View(imputationViewModel);
            }

            //Se retorna a la vista con el modelo vacío y se envía el valor del success para manejar los mensajes
            ViewBag.IsSuccess = response.IsSuccess.ToString();
            ViewBag.User = User.Identity.Name;

            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobsServices(ViewBag.User);
            hourImputations.HourImputationFilter.InputDate = DateTime.Now;

            return View(hourImputations);
        }
        #endregion

        #region Jobs
        private HourImputationFilter GetJobsServices(string user)
        {
            var hourImputation = new HourImputationFilter();
            var jobs = CommonServices.GetJobsExpenses(user);

            hourImputation.JobsCollection = jobs;
            return hourImputation;
        }

        [HttpPost]
        public PartialViewResult GetJobsExpensesFinished(string user)
        {
            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobsExpensesFinishedServices(user);
            return PartialView("JobPartial", hourImputations);
        }

        private HourImputationFilter GetJobsExpensesFinishedServices(string user)
        {
            var hourImputation = new HourImputationFilter();
            var jobsFinished = CommonServices.GetJobsExpensesFinished(user);
            hourImputation.JobsCollection = jobsFinished;
            return hourImputation;
        }

        [HttpPost]
        public PartialViewResult GetCurrentJobsExpenses(string user)
        {
            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobsServices(user);
            return PartialView("JobPartial", hourImputations);
        }

        #endregion

        #region Tasks
        [HttpPost]
        public PartialViewResult GetJobTasksExpenses(string project, string user)
        {
            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetJobTaskExpensesServices(project, user);

            return PartialView("TaskPartial", hourImputations);
        }

        private HourImputationFilter GetJobTaskExpensesServices(string project, string user)
        {
            var hourImputation = new HourImputationFilter();
            var jobTasks = CommonServices.GetJobTasksExpenses(project, user);
            hourImputation.JobsTaskCollection = jobTasks;
            hourImputation.ResponsiblePerson = CommonServices.GetResponsible(project);

            return hourImputation;
        }

        #endregion

        #region PlanningLines
        [HttpPost]
        public PartialViewResult GetPlanningLinesExpenses(string project, string task, string user)
        {
            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputationFilter = GetPlanningLinesExpensesServices(project, task, user);

            return PartialView("PlanningLinesPartial", hourImputations);
        }

        private HourImputationFilter GetPlanningLinesExpensesServices(string project, string task, string user)
        {
            var hourImputation = new HourImputationFilter();
            var planningLines = CommonServices.GetPlanningLinesExpenses(project, task, user);
            hourImputation.PlanningLinesCollection = planningLines;
            return hourImputation;
        }

        #endregion

        #region LoadPartialView
        [HttpPost]
        public ActionResult AddImputationExpensesLine(DateTime inputDate, string project, string projectDescription,
            string task, string taskDescription, int planningLineNo, string planningLineDescription, string worktypecode,
            string workTypeDescription, string UoM, decimal qty, decimal price, string user, string responsible)
        {
            var hourImputationLine = new HourImputationPreLines
            {
                InputDate = inputDate,
                ProjectNo = project,
                ProjectDescription = projectDescription,
                ProjectTaskNo = task,
                ProjectTaskDescription = taskDescription,
                PlanningLineNo = planningLineNo,
                PlanningLineDescription = planningLineDescription,
                WorkType = worktypecode,
                WorkTypeDescription = workTypeDescription,
                UnitOfMeasure = UoM,
                Price = price,
                Qty = qty,
                User = user,
                ResponsiblePerson = user
            };

            var response = HourImputationServices.AddImputationHourLine(hourImputationLine);

            if (!response.IsSuccess)
            {
                return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }

            var hourImputations = new HourImputationViewModel();

            hourImputations.HourImputations = 
                HourImputationServices.GetImputationHourPreLines(user, true);            

            return PartialView("_ExpensesImputationsList", hourImputations);
        }

        [HttpPost]
        public ActionResult DeleteImputationExpensesLine(int id)
        {
            var user = User.Identity.Name;

            var response = HourImputationServices.DeleteImputationHourLine(id);

            //if (!response.IsSuccess)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            //}

            var hourImputations = new HourImputationViewModel();
            hourImputations.HourImputations =
                HourImputationServices.GetImputationHourPreLines(user, true);

            return PartialView("_ExpensesImputationsList", hourImputations);
        }
        #endregion
    }
}