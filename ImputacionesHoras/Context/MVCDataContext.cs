﻿using ImputacionesHorasDomain.Context;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ImputacionesHoras.Context
{
    public class MVCDataContext:DataContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}