﻿using System.Net.Mail;
using System.Collections.Generic;
using ImputacionesHorasCommon.Helpers;
using System.Text;
using System.Net.Mime;
using System.Net;

namespace ImputacionesHoras.Helpers
{
    public class MailHelper
    {
        #region Atributos
        private static string htmlBody;
        private static string planeBody;
        private static string merror;
        #endregion

        #region Propiedades
        public string Error_message
        {
            get { return merror; }
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Método para el envío de correos.
        /// </summary>
        /// <param name="tipo">0 = Registro de nuevo usuario.</param>
        /// <param name="mails"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public static bool SendMail(ImputacionesCommonHelper.EmailType tipo, List<string> mails, 
            string Usuario, string UsuarioID = null, string Password = null)
        {
            MailMessage mail = new MailMessage();
            SmtpClient protocol = new SmtpClient();

            //Se buscan las direcciones a enviar correo
            foreach (string mailDir in mails)
            {
                mail.To.Add(new MailAddress(mailDir));
            }

            //Se busca el tipo de solicitud que se está haciendo para armar el cuerpo del correo, plano o html.
            switch (tipo)
            {
                case ImputacionesCommonHelper.EmailType.Usuario:
                    htmlBody = "<html> "
                             + "<style type=\"text/css\"> "
                             + " body {font-family:Microsoft JhengHei, Arial;} "
                             + "</style> "
                             + "<body> "
                             + "<p>Sr(a). <b>" + Usuario + ",</b><br/><br/>"
                             + "Se ha creado un acceso al sistema para la imputación de horas. <br/>"
                             + "Sus datos de acceso, son los siguientes: <br/>"
                             + "Usuario: <b>" + UsuarioID + "</b><br/> "
                             + "Contraseña: <b>" + Password + "</b><br/><br/> "
                             + "Si ha recibido éste mail por error, por favor, notificar al departamento encargado.</p> <br/><br/><br/><br/> "
                             + "<hr>"
                             + "<p><b>Correo Electrónico enviado por el sistema de manera automática.-</b></p>"
                             + " </body> "
                             + "</html>";
                    planeBody = "Sr(a). " + Usuario + ". "
                              + "Se ha creado un acceso al sistema para la imputación de horas. Sus "
                              + "datos de acceso, son los siguientes: Usuario: " + Usuario + " Contraseña: " + UsuarioID + " "
                              + "Si ha recibido éste mail por error, por favor, notificar al departamento encargado."
                              + "Correo electrónico enviado por el sistema de manera automática.-";

                    break;
            }

            //Se define la dirección desde dónde se hará el envío.
            mail.From = (new MailAddress("elnueve80@gmail.com", "Imputaciones de horas", Encoding.UTF8));

            //Se establece el subject del correo según el tipo de solicitud.
            switch (tipo)
            {
                case ImputacionesCommonHelper.EmailType.Usuario:
                    mail.Subject = "Registro de usuario " + Usuario + " para imputaciones de horas.";
                    break;
            }

            //Se establece la codificación del subject.
            mail.SubjectEncoding = Encoding.UTF8;

            //Se establecen las diferentes vistas (html - plain)
            AlternateView plainView = AlternateView.CreateAlternateViewFromString(planeBody, Encoding.UTF8, MediaTypeNames.Text.Plain);
            AlternateView htmlView = AlternateView.CreateAlternateViewFromString(htmlBody, Encoding.UTF8, MediaTypeNames.Text.Html);

            mail.AlternateViews.Add(plainView);
            mail.AlternateViews.Add(htmlView);
            mail.IsBodyHtml = true;

            //Se configura el protocolo de envío del correo.
            protocol.Credentials = new NetworkCredential("elnueve80@gmail.com", "Lumia@550");
            protocol.Port = 587;
            protocol.Host = "smtp.gmail.com";
            protocol.EnableSsl = true;

            //Se envía el correo.
            try
            {
                protocol.Send(mail);
                return true;
            }
            catch (SmtpException error)
            {
                merror = error.InnerException.Message.ToString();
                return false;
            }
        }
        #endregion
    }
}