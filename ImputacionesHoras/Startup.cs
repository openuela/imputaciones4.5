﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ImputacionesHoras.Startup))]
namespace ImputacionesHoras
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
